#!/usr/bin/env python

import time
import threading
import signal
import sys

import roslib; roslib.load_manifest('gcs_gui_demo')
import rospy

from mav_interface import *
from gcs_gui import *


gcs = None

#-------------------------------------------------------------------------------

class GCSMain(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.gui = None
        self.mavint = None

    def run(self):
        counter = 0
        r = rospy.Rate(10)
        while (not rospy.is_shutdown()):
            if (counter == 10):
                #rospy.loginfo("*GCS*")
                self.mavint.periodicCommsCheck()
                counter = 0
            if (self.mavint.gcsTeleopActive):
                self.mavint.sendTeleopCmd()
            counter = counter + 1
            r.sleep()
        if (self.gui != None):    
            self.gui.app.exit()
        
    def stop(self):
        if (self.gui != None):
            self.gui.app.exit()
        rospy.signal_shutdown("closing application")


#-------------------------------------------------------------------------------
def sigint_handler(*args):
    """Handler for the SIGINT signal."""
    try:
        rospy.loginfo('signal captured')
        if (gcs != None):
            gcs.stop()
        rospy.signal_shutdown("sigint received")
    except:
        pass


#===============================================================================

def main():
    rospy.init_node('gcs_gui_demo')
    rospy.loginfo('gcs_gui_demo - START')

    gcs = GCSMain()
    
    gcs.mavint = MAVInterface()
    gcs.gui = GcsGui(gcs.mavint)

    gcs.start()

    gcs.gui.run()
    
    gcs.stop()
    
    time.sleep(1)
    rospy.loginfo('gcs_gui_demo - END')

#===============================================================================

if __name__ == '__main__':
    try:
        signal.signal(signal.SIGINT, sigint_handler)
        main()
    except rospy.ROSInterruptException:
        pass

