#!/usr/bin/env python

import math
import time
import thread

import roslib; roslib.load_manifest('gcs_gui_demo')
import rospy

from std_msgs.msg import *
from sensor_msgs.msg import *
from mav_gcs_msgs.msg import *
from ca_nav_msgs.msg import *


#===============================================================================

class MapJoyToTeleop:
    def __init__(self):
        self.axes_x  = 1 # forward/backward [1 / -1]
        self.axes_y  = 0 # left/right [1 / -1]

        self.button_up      = 2 # up     1
        self.button_down    = 1 # down  -1
        self.button_yaw_cw  = 0 # yaw clock-wise           1
        self.button_yaw_ccw = 3 # yaw counter-clock-wise  -1

    def convert(self, joy):
        teleop = TeleopCmd()
        teleop.cmd_x = 0
        teleop.cmd_y = 0
        teleop.cmd_z = 0
        teleop.cmd_yaw = 0
        if (joy.axes[self.axes_x] == 1.0):
            teleop.cmd_x = 1.0
        elif (joy.axes[self.axes_x] == -1.0):
            teleop.cmd_x = -1.0
        if (joy.axes[self.axes_y] == 1.0):
            teleop.cmd_y = 1.0
        elif (joy.axes[self.axes_y] == -1.0):
            teleop.cmd_y = -1.0
        if ((joy.buttons[self.button_up] == 1) and (joy.buttons[self.button_down] == 0)):
            teleop.cmd_z = -1.0
        elif ((joy.buttons[self.button_down] == 1) and (joy.buttons[self.button_up] == 0)):
            teleop.cmd_z = 1.0
        if ((joy.buttons[self.button_yaw_cw] == 1) and (joy.buttons[self.button_yaw_ccw] == 0)):
            teleop.cmd_yaw = 1.0
        elif ((joy.buttons[self.button_yaw_ccw] == 1) and (joy.buttons[self.button_yaw_cw] == 0)):
            teleop.cmd_yaw = -1.0
        return teleop

#===============================================================================

class MAVInterface:

    def __init__(self):
        rospy.loginfo("MAVInterface")
        self.currMavStatus = None        
        self.timeRecvMavStatus = None
        self.currMavHwStatus = None        
        self.timeRecvMavHwStatus = None

        self.statusCounter = 0
        self.missionCounter = 0
        self.teleopCounter = 0
        self.userCmdCounter = 0
        
        self.mavCommsTimeout = 5.0
        self.lostCommsMAV = False

        self.currMavTaskStatus = None

        # MAV state variables
        self.mavIsOnGround = False
        self.mavPrevState  = FCSStatus.ONGROUND
        
        # GCS state variables
        self.gcsTeleopActive = False
        self.gcsHasMission   = False
        self.gcsMission = None
                
        self.gcsMissionPub   = rospy.Publisher('/gcs/mission', Mission, queue_size = 1)
        self.gcsTeleopCmdPub = rospy.Publisher('/gcs/teleop_cmd', TeleopCmd, queue_size = 1)
        self.gcsUserCmdPub   = rospy.Publisher('/fcs/user_cmd', UserCmd, queue_size = 1)
        self.gcsWaypointsPub = rospy.Publisher('/fcs/path', PathXYZVPsi, queue_size = 1)

        self.mavStatusSub    = rospy.Subscriber('/fcs/fcs_status', FCSStatus, self.mavStatusHandler, queue_size = 1)
        self.mavHwStatusSub  = rospy.Subscriber('/mav/hardware_status', HardwareStatus, self.mavHwStatusHandler, queue_size = 1)
        self.mavTaskStatusSub  = rospy.Subscriber('/fcs/task_status', Float32MultiArray, self.mavTaskStatusHandler, queue_size = 1)

        self.mapJoyToTeleop = MapJoyToTeleop()
        self.currJoyCmd = None
        self.joySub = rospy.Subscriber("/joy", Joy, self.joyHandler)
       
        
    def getAirLabLogoImageFilename(self):
        return rospy.get_param('/gcs_gui_demo/airlab_logo_image')
        

    def getInspectionAreaImageFilenames(self):
        filename1 = rospy.get_param('/gcs_gui_demo/inspection_area1_image')
        filename2 = rospy.get_param('/gcs_gui_demo/inspection_area2_image')
        rospy.loginfo(filename1)
        rospy.loginfo(filename2)
        return [filename1, filename2]


    def setTeleopState(self, flag):
        self.gcsTeleopActive = flag


    def mavStatusHandler(self, msg):
        #rospy.loginfo("MAVInterface::mavStatusHandler()")
        self.currMavStatus = msg
        self.timeRecvMavStatus = rospy.Time.now().to_sec()
        if (self.currMavStatus.state == FCSStatus.ONGROUND):
            self.mavIsOnGround = True
        else:
            self.mavIsOnGround = False
            
        if ((self.gcsHasMission) and (self.mavPrevState == FCSStatus.EXECUTINGCOMMAND) and (self.currMavStatus.state == FCSStatus.HOLDINGPOSITION)):
            self.gcsHasMission = False
            self.gcsMission = None
        
        if (self.mavPrevState != self.currMavStatus.state):
            self.mavPrevState = self.currMavStatus.state


    def mavHwStatusHandler(self, msg):
        #rospy.loginfo("MAVInterface::mavHwStatusHandler()")
        self.currMavHwStatus = msg
        self.timeRecvMavHwStatus = rospy.Time.now().to_sec()


    def mavTaskStatusHandler(self, msg):
        self.currMavTaskStatus = msg


    def joyHandler(self, msg):
        #rospy.loginfo("MAVInterface::joyHandler()")
        self.currJoyCmd = msg


    def sendMission(self):
        mission = Mission()
        mission.header.stamp = rospy.Time.now()
        mission.index = self.missionCounter
        self.gcsMissionPub.publish(mission)
        self.missionCounter = self.missionCounter + 1 


    def sendTeleopCmd(self):
        teleopcmd = TeleopCmd()
        if (self.currJoyCmd != None):
            teleopcmd = self.mapJoyToTeleop.convert(self.currJoyCmd)
        teleopcmd.header.stamp = rospy.Time.now()
        self.gcsTeleopCmdPub.publish(teleopcmd)
        self.teleopCounter = self.teleopCounter + 1


    def sendUserCmd(self, cmd):
        usercmd = UserCmd()
        usercmd.header.stamp = rospy.Time.now()
        usercmd.index = self.userCmdCounter
        usercmd.user_cmd = cmd
        self.gcsUserCmdPub.publish(usercmd)
        self.userCmdCounter = self.userCmdCounter + 1 


    def periodicCommsCheck(self):
        if ((self.timeRecvMavStatus == None) and (self.timeRecvMavHwStatus == None)):
            rospy.logwarn("MAVInterface - no message received from MAV.")
            return

        now = rospy.Time.now().to_sec()
        deltaTime = self.mavCommsTimeout / 2.0
        if (self.timeRecvMavStatus != None):
            dt = now - self.timeRecvMavStatus
            if (deltaTime > dt):
                deltaTime = dt
        if (self.timeRecvMavHwStatus != None):
            dt = now - self.timeRecvMavHwStatus
            if (deltaTime > dt):
                deltaTime = dt
        
        if (deltaTime > self.mavCommsTimeout):
            rospy.logerr("MAVInterface - lost COMMS with MAV.")
            self.lostCommsMAV = True
        elif (self.lostCommsMAV):
            rospy.loginfo("MAVInterface - recovered COMMS with MAV.")
            self.lostCommsMAV = False


    def sendUserCmdTeleopOn(self):
        self.sendUserCmd(UserCmd.CMD_TELEOP_ON)

    def sendUserCmdTeleopOff(self):
        self.sendUserCmd(UserCmd.CMD_TELEOP_OFF)

    def sendUserCmdTakeoff(self):
        self.sendUserCmd(UserCmd.CMD_TAKEOFF)

    def sendUserCmdLand(self):
        self.sendUserCmd(UserCmd.CMD_LAND)

    def sendUserCmdStart(self):
        self.sendUserCmd(UserCmd.CMD_MISSION_START)

    def sendUserCmdStop(self):
        self.sendUserCmd(UserCmd.CMD_MISSION_STOP)

    def sendUserCmdPause(self):
        self.sendUserCmd(UserCmd.CMD_MISSION_PAUSE)

    def sendUserCmdResume(self):
        self.sendUserCmd(UserCmd.CMD_MISSION_RESUME)


    #===========================================================================
    #===========================================================================

    '''
    #Scan ceiling - horizontal plane
    def sendPathSection(self, section):
        rospy.loginfo("MAVInterface - sending points for are section number={0}".format(section))
        path = None
        height_inspect = -1.5
        hover_point = [0.0, 0.0, -1.5, 0]
        side = 1.0
        if (section == 0):
            path = self.createPath([hover_point, 
                                    [ 4*side,-2*side, height_inspect, 0],
                                    [ 4*side,-1*side, height_inspect, 0],
                                    [ 4*side, 1*side, height_inspect, 0],
                                    [ 4*side, 2*side, height_inspect, 0],
                                    [ 3*side, 2*side, height_inspect, 0],
                                    [ 3*side, 1*side, height_inspect, 0],
                                    [ 3*side,-1*side, height_inspect, 0],
                                    [ 3*side,-2*side, height_inspect, 0],
                                    [ 2*side,-2*side, height_inspect, 0],
                                    [ 2*side,-1*side, height_inspect, 0],
                                    [ 2*side, 1*side, height_inspect, 0],
                                    [ 2*side, 2*side, height_inspect, 0],
                                    [ 1*side, 2*side, height_inspect, 0],
                                    [ 1*side, 1*side, height_inspect, 0],
                                    [ 1*side,-1*side, height_inspect, 0],
                                    [ 1*side,-2*side, height_inspect, 0],
                                    hover_point])
        elif (section == 1):
            path = self.createPath([hover_point, [ 4*side,-2*side, height_inspect, -1], hover_point])
        elif (section == 2):
            path = self.createPath([hover_point, [ 4*side,-1*side, height_inspect, -1], hover_point])
        elif (section == 3):
            path = self.createPath([hover_point, [ 4*side, 1*side, height_inspect, -1], hover_point])
        elif (section == 4):
            path = self.createPath([hover_point, [ 4*side, 2*side, height_inspect, -1], hover_point])
        elif (section == 5):
            path = self.createPath([hover_point, [ 3*side,-2*side, height_inspect, -1], hover_point])
        elif (section == 6):
            path = self.createPath([hover_point, [ 3*side,-1*side, height_inspect, -1], hover_point])
        elif (section == 7):
            path = self.createPath([hover_point, [ 3*side, 1*side, height_inspect, -1], hover_point])
        elif (section == 8):
            path = self.createPath([hover_point, [ 3*side, 2*side, height_inspect, -1], hover_point])
        elif (section == 9):
            path = self.createPath([hover_point, [ 2*side,-2*side, height_inspect, -1], hover_point])
        elif (section == 10):
            path = self.createPath([hover_point, [ 2*side,-1*side, height_inspect, -1], hover_point])
        elif (section == 11):
            path = self.createPath([hover_point, [ 2*side, 1*side, height_inspect, -1], hover_point])
        elif (section == 12):
            path = self.createPath([hover_point, [ 2*side, 2*side, height_inspect, -1], hover_point])
        elif (section == 13):
            path = self.createPath([hover_point, [ 1*side,-2*side, height_inspect, -1], hover_point])
        elif (section == 14):
            path = self.createPath([hover_point, [ 1*side,-1*side, height_inspect, -1], hover_point])
        elif (section == 15):
            path = self.createPath([hover_point, [ 1*side, 1*side, height_inspect, -1], hover_point])
        elif (section == 16):
            path = self.createPath([hover_point, [ 1*side, 2*side, height_inspect, -1], hover_point])
        else:
            path = None

        if (path != None):
            self.gcsWaypointsPub.publish(path)
            self.gcsHasMission = True
    '''

    '''
    #Scan wall - vertical plane
    def sendPathSection(self, section):
        rospy.loginfo("MAVInterface - sending points for are section number={0}".format(section))
        path = None
        base_height = 0.0
        plane_inspect = 2.0
        hover_point = [0.0, 0.0, -1.0, 0]
        side = 1.0
        if (section == 0):
            path = self.createPath([hover_point, 
                                    [ plane_inspect,-1.5*side, base_height-4*side, 0],
                                    [ plane_inspect,-0.5*side, base_height-4*side, 0],
                                    [ plane_inspect, 0.5*side, base_height-4*side, 0],
                                    [ plane_inspect, 1.5*side, base_height-4*side, 0],
                                    [ plane_inspect, 1.5*side, base_height-3*side, 0],
                                    [ plane_inspect, 0.5*side, base_height-3*side, 0],
                                    [ plane_inspect,-0.5*side, base_height-3*side, 0],
                                    [ plane_inspect,-1.5*side, base_height-3*side, 0],
                                    [ plane_inspect,-1.5*side, base_height-2*side, 0],
                                    [ plane_inspect,-0.5*side, base_height-2*side, 0],
                                    [ plane_inspect, 0.5*side, base_height-2*side, 0],
                                    [ plane_inspect, 1.5*side, base_height-2*side, 0],
                                    [ plane_inspect, 1.5*side, base_height-1*side, 0],
                                    [ plane_inspect, 0.5*side, base_height-1*side, 0],
                                    [ plane_inspect,-0.5*side, base_height-1*side, 0],
                                    [ plane_inspect,-1.5*side, base_height-1*side, 0],
                                    hover_point])
        elif (section == 1):
            path = self.createPath([hover_point, [ plane_inspect,-1.5*side, base_height-4*side, -1], hover_point])
        elif (section == 2):
            path = self.createPath([hover_point, [ plane_inspect,-0.5*side, base_height-4*side, -1], hover_point])
        elif (section == 3):
            path = self.createPath([hover_point, [ plane_inspect, 0.5*side, base_height-4*side, -1], hover_point])
        elif (section == 4):
            path = self.createPath([hover_point, [ plane_inspect, 1.5*side, base_height-4*side, -1], hover_point])
        elif (section == 5):
            path = self.createPath([hover_point, [ plane_inspect,-1.5*side, base_height-3*side, -1], hover_point])
        elif (section == 6):
            path = self.createPath([hover_point, [ plane_inspect,-0.5*side, base_height-3*side, -1], hover_point])
        elif (section == 7):
            path = self.createPath([hover_point, [ plane_inspect, 0.5*side, base_height-3*side, -1], hover_point])
        elif (section == 8):
            path = self.createPath([hover_point, [ plane_inspect, 1.5*side, base_height-3*side, -1], hover_point])
        elif (section == 9):
            path = self.createPath([hover_point, [ plane_inspect,-1.5*side, base_height-2*side, -1], hover_point])
        elif (section == 10):
            path = self.createPath([hover_point, [ plane_inspect,-0.5*side, base_height-2*side, -1], hover_point])
        elif (section == 11):
            path = self.createPath([hover_point, [ plane_inspect, 0.5*side, base_height-2*side, -1], hover_point])
        elif (section == 12):
            path = self.createPath([hover_point, [ plane_inspect, 1.5*side, base_height-2*side, -1], hover_point])
        elif (section == 13):
            path = self.createPath([hover_point, [ plane_inspect,-1.5*side, base_height-1*side, -1], hover_point])
        elif (section == 14):
            path = self.createPath([hover_point, [ plane_inspect,-0.5*side, base_height-1*side, -1], hover_point])
        elif (section == 15):
            path = self.createPath([hover_point, [ plane_inspect, 0.5*side, base_height-1*side, -1], hover_point])
        elif (section == 16):
            path = self.createPath([hover_point, [ plane_inspect, 1.5*side, base_height-1*side, -1], hover_point])
        else:
            path = None

        if (path != None):
            self.gcsWaypointsPub.publish(path)
            self.gcsHasMission = True
    '''
 
    '''   
    #Scan wall - vertical plane (2 walls - Frontiers Conference Demo)
    def setPathSection(self, area, section):
        rospy.loginfo("MAVInterface - sending points for are area={0} / section={1}".format(area, section))
        
        path = None
        
        #Each point has: X, Y, Z, YAW and pause flag
        hover_point = [0.0, 0.0, -1.5, 0.0, 0]

        base_height = -1.5
        plane_inspect_x = 2.0
        plane_inspect_y = 2.0
        sideh = 1.0
        sidev = 0.75
        
        # Create paths for area1 
        if (area == 1):
            heading = 0.0
            if (section == 0):
                path = self.createPath([hover_point,
                                    [ plane_inspect_x,-2.0*sideh, base_height-2*sidev, heading, 0], #1
                                    [ plane_inspect_x, 2.0*sideh, base_height-2*sidev, heading, 0], #4
                                    [ plane_inspect_x, 2.0*sideh, base_height-1*sidev, heading, 0], #8
                                    [ plane_inspect_x,-2.0*sideh, base_height-1*sidev, heading, 0], #5
                                    [ plane_inspect_x,-2.0*sideh, base_height-0*sidev, heading, 0], #9
                                    [ plane_inspect_x, 2.0*sideh, base_height-0*sidev, heading, 0], #12
                                    hover_point])
            elif (section == 1):
                path = self.createPath([hover_point, [ plane_inspect_x,-2.0*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 2):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.0*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 3):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.0*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 4):
                path = self.createPath([hover_point, [ plane_inspect_x, 1.0*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 5):
                path = self.createPath([hover_point, [ plane_inspect_x,-2.0*sideh, base_height-1*sidev, heading, -1], hover_point])

            elif (section == 6):
                path = self.createPath([hover_point, [ plane_inspect_x,-2.0*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 7):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.0*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 8):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.0*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 9):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.0*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 10):
                path = self.createPath([hover_point, [ plane_inspect_x,-2.0*sideh, base_height-0*sidev, heading, -1], hover_point])

            elif (section == 11):
                path = self.createPath([hover_point, [ plane_inspect_x,-2.0*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 12):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.0*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 13):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.0*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 14):
                path = self.createPath([hover_point, [ plane_inspect_x, 1.0*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 15):
                path = self.createPath([hover_point, [ plane_inspect_x, 2.0*sideh, base_height-0*sidev, heading, -1], hover_point])

            else:
                path = None

        #Create paths for area 2
        if (area == 2):
            heading = 90.0
            if (section == 0):
                path = self.createPath([hover_point,
                                    [ 2.0*sideh, plane_inspect_y, base_height-2*sidev, heading, 0], #1
                                    [-2.0*sideh, plane_inspect_y, base_height-2*sidev, heading, 0], #4
                                    [-2.0*sideh, plane_inspect_y, base_height-1*sidev, heading, 0], #8
                                    [ 2.0*sideh, plane_inspect_y, base_height-1*sidev, heading, 0], #5
                                    [ 2.0*sideh, plane_inspect_y, base_height-0*sidev, heading, 0], #9
                                    [-2.0*sideh, plane_inspect_y, base_height-0*sidev, heading, 0], #12
                                    hover_point])
            elif (section == 1):
                path = self.createPath([hover_point, [ 2.0*sideh, plane_inspect_y, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 2):
                path = self.createPath([hover_point, [ 1.0*sideh, plane_inspect_y, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 3):
                path = self.createPath([hover_point, [ 0.0*sideh, plane_inspect_y, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 4):
                path = self.createPath([hover_point, [-1.0*sideh, plane_inspect_y, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 5):
                path = self.createPath([hover_point, [-2.0*sideh, plane_inspect_y, base_height-1*sidev, heading, -1], hover_point])

            elif (section == 6):
                path = self.createPath([hover_point, [ 2.0*sideh, plane_inspect_y, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 7):
                path = self.createPath([hover_point, [ 1.0*sideh, plane_inspect_y, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 8):
                path = self.createPath([hover_point, [ 0.0*sideh, plane_inspect_y, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 9):
                path = self.createPath([hover_point, [-1.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 10):
                path = self.createPath([hover_point, [-2.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])

            elif (section == 11):
                path = self.createPath([hover_point, [ 2.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 12):
                path = self.createPath([hover_point, [ 1.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 13):
                path = self.createPath([hover_point, [ 0.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 14):
                path = self.createPath([hover_point, [-1.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 15):
                path = self.createPath([hover_point, [-2.0*sideh, plane_inspect_y, base_height-0*sidev, heading, -1], hover_point])

            else:
                path = None
        

        if (path != None):
            self.gcsHasMission = True
            self.gcsMission = path


    def createPath(self, points):
        rotation_angle = -45
        angle_rad = rotation_angle * (math.pi/180.0)
        
        path = PathXYZVPsi()
    
        path.header.seq = 1
        path.header.stamp = rospy.Time.now()
        path.header.frame_id = '/world'

        size = len(points)

        pnt = points[0]
        wp = XYZVPsi()
        wp.position.x = pnt[0]
        wp.position.y = pnt[1]
        wp.position.z = pnt[2]
        wp.heading    = pnt[3]
        wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
        path.waypoints.append(wp)
        rospy.logerr(wp)
      
        index = 1
        while (index < (size-1)):
            pnt = points[index]
            index = index + 1  
            wp = XYZVPsi()
            wp.position.x = pnt[0] * math.cos(angle_rad) - pnt[1] * math.sin(angle_rad)
            wp.position.y = pnt[1] * math.cos(angle_rad) + pnt[0] * math.sin(angle_rad)
            wp.position.z = pnt[2]
            wp.heading    = (pnt[3] + rotation_angle) * (math.pi/180.0)
            wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
            path.waypoints.append(wp)
            rospy.logerr(wp)

        pnt = points[size-1]
        wp = XYZVPsi()
        wp.position.x = pnt[0]
        wp.position.y = pnt[1]
        wp.position.z = pnt[2]
        wp.heading    = pnt[3]
        wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
        path.waypoints.append(wp)
        rospy.logerr(wp)

        return path
    '''
    
    #Scan wall - vertical plane (high bay roller door - Mitsubishi Demo)
    def setPathSection(self, area, section):
        rospy.logerr("MAVInterface - sending points for are area={0} / section={1}".format(area, section))
        
        path = None
        
        #Each point has: X, Y, Z, YAW and pause flag
        hover_point = [0.0, 0.0, -1.5, 0.0, 0]

        base_height = -1.5
        plane_inspect_x = 3.5
        plane_inspect_y = 2.0
        sideh = 0.75
        sidev = 0.75
        
        # Create paths for area
        if (area == 1):
            heading = 0.0
            if (section == 0):
                path = self.createPath([hover_point,
                                    [ plane_inspect_x,-1.5*sideh, base_height-2*sidev, heading, 0], #1
                                    [ plane_inspect_x, 1.5*sideh, base_height-2*sidev, heading, 0], #4
                                    [ plane_inspect_x, 1.5*sideh, base_height-1*sidev, heading, 0], #8
                                    [ plane_inspect_x,-1.5*sideh, base_height-1*sidev, heading, 0], #5
                                    [ plane_inspect_x,-1.5*sideh, base_height-0*sidev, heading, 0], #9
                                    [ plane_inspect_x, 1.5*sideh, base_height-0*sidev, heading, 0], #12
                                    hover_point])
            elif (section == 1):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.5*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 2):
                path = self.createPath([hover_point, [ plane_inspect_x,-0.5*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 3):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.5*sideh, base_height-2*sidev, heading, -1], hover_point])
            elif (section == 4):
                path = self.createPath([hover_point, [ plane_inspect_x, 1.5*sideh, base_height-2*sidev, heading, -1], hover_point])

            elif (section == 5):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.5*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 6):
                path = self.createPath([hover_point, [ plane_inspect_x,-0.5*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 7):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.5*sideh, base_height-1*sidev, heading, -1], hover_point])
            elif (section == 8):
                path = self.createPath([hover_point, [ plane_inspect_x, 1.5*sideh, base_height-1*sidev, heading, -1], hover_point])

            elif (section == 9):
                path = self.createPath([hover_point, [ plane_inspect_x,-1.5*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 10):
                path = self.createPath([hover_point, [ plane_inspect_x,-0.5*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 11):
                path = self.createPath([hover_point, [ plane_inspect_x, 0.5*sideh, base_height-0*sidev, heading, -1], hover_point])
            elif (section == 12):
                path = self.createPath([hover_point, [ plane_inspect_x, 1.5*sideh, base_height-0*sidev, heading, -1], hover_point])

            else:
                path = None

        

        if (path != None):
            self.gcsHasMission = True
            self.gcsMission = path


    def createPath(self, points):
        rotation_angle = 0.0
        angle_rad = rotation_angle * (math.pi/180.0)
        
        path = PathXYZVPsi()
    
        path.header.seq = 1
        path.header.stamp = rospy.Time.now()
        path.header.frame_id = '/world'

        size = len(points)

        pnt = points[0]
        wp = XYZVPsi()
        wp.position.x = pnt[0]
        wp.position.y = pnt[1]
        wp.position.z = pnt[2]
        wp.heading    = pnt[3]
        wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
        path.waypoints.append(wp)
        #rospy.logerr(wp)
      
        index = 1
        while (index < (size-1)):
            pnt = points[index]
            index = index + 1  
            wp = XYZVPsi()
            wp.position.x = pnt[0] * math.cos(angle_rad) - pnt[1] * math.sin(angle_rad)
            wp.position.y = pnt[1] * math.cos(angle_rad) + pnt[0] * math.sin(angle_rad)
            wp.position.z = pnt[2]
            wp.heading    = (pnt[3] + rotation_angle) * (math.pi/180.0)
            wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
            path.waypoints.append(wp)
            #rospy.logerr(wp)

        pnt = points[size-1]
        wp = XYZVPsi()
        wp.position.x = pnt[0]
        wp.position.y = pnt[1]
        wp.position.z = pnt[2]
        wp.heading    = pnt[3]
        wp.vel        = pnt[4] #@TODO: this field is used to tell motion control to keep going or make a short stop
        path.waypoints.append(wp)
        #rospy.logerr(wp)

        return path
    

    def sendPath(self):
        if ((self.gcsHasMission) and (self.gcsMission != None)):
            self.gcsWaypointsPub.publish(self.gcsMission)


