#!/usr/bin/env python
import sys
import math
import time

from PyQt4 import QtCore
from PyQt4 import QtGui

from mav_gcs_msgs.msg import *

#===============================================================================

def getOpModeStr(opmode):
    #rospy.loginfo(opmode)
    codeDict = { FCSStatus.MANUAL : 'MANUAL',
                 FCSStatus.AUTONOMOUS : 'AUTONOMOUS'}
    return codeDict.get(opmode, 'UNKNOWN')


def getFlightStateStr(state):
    #rospy.loginfo(state)
    codeDict = { FCSStatus.ONGROUND : 'ON GROUND',
                 FCSStatus.TAKINGOFF : 'TAKING OFF',
                 FCSStatus.LANDING : 'LANDING',
                 FCSStatus.HOLDINGPOSITION : 'HOVER POSITION',
                 FCSStatus.EXECUTINGCOMMAND :'EXECUTING TASK' }    
    return codeDict.get(state, 'UNKNOWN')

#===============================================================================

class GcsHardwareStatusPanel:

    def __init__(self, gcsGui):
        self.gcsgui = gcsGui

        self.widget = QtGui.QWidget()
        self.widget.setWindowTitle("AirLab GCS - System Status") 
        self.widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)
                
        self.batteryLevelLabel = QtGui.QLabel('Battery: ??')
        self.cpuUsageLabel = QtGui.QLabel('CPU: ??')
        self.ramUsageLabel = QtGui.QLabel('RAM: ??')
        self.storageUsageLabel = QtGui.QLabel('DISK: ??')

        f = QtGui.QFont("Arial", 20, QtGui.QFont.Bold)
        self.batteryLevelLabel.setFont(f)

        f = QtGui.QFont("Arial", 12, QtGui.QFont.Bold)
        self.cpuUsageLabel.setFont(f)
        self.ramUsageLabel.setFont(f)
        self.storageUsageLabel.setFont(f)

        grid = QtGui.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(self.batteryLevelLabel, 1, 0, 1, 3)
        grid.addWidget(self.cpuUsageLabel, 2, 0, 1, 1)
        grid.addWidget(self.ramUsageLabel, 2, 1, 1, 1)
        grid.addWidget(self.storageUsageLabel, 2, 2, 1, 1)

        self.widget.setLayout(grid)
        
        #self.widget.resize(400, 240)
                
        self.widget.show()

        # Register action to close using CTRL+C 
        self.closeAction = QtGui.QAction('Close', self.widget)
        self.closeAction.setShortcut('Ctrl+C')
        self.closeAction.setStatusTip('Close application')
        self.closeAction.triggered.connect(self.close)

        self.bgColorRed    = "{r}, {g}, {b}, {a}".format(r=255, g=0, b=0, a=127)
        self.bgColorYellow = "{r}, {g}, {b}, {a}".format(r=255, g=255, b=0, a=127)
        self.bgColorGreen  = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)
        
        #Implementing timer to update interface
        self.ctimer = QtCore.QTimer()
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.update)
        QtCore.QMetaObject.connectSlotsByName(self.widget)
        self.ctimer.start(100)


    def update(self):
        #print 'GcsHardwareStatusPanel::update()'
        #if (self.gcsgui.mavint.currMavHwStatus == None):
        #    return
        self.updateBatteryLevel()
        self.updateOnboardCPUStatus()


    def close(self):
        print 'GcsHardwareStatusPanel::close()'
        self.gcsgui.app.exit()

    #---------------------------------------------------------------------------
    # Auxiliar methods for interface update

    def updateLabelColorValueRange(self, label, text, value, thresh1, thresh2):
        label.setText(text)
        label.setAutoFillBackground(True)
        if (value < thresh1):
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")
        elif ((value >= thresh1) and (value < thresh2)):
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
        else:
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorRed+"); }")


    def updateLabelColorValueInvertRange(self, label, text, value, thresh1, thresh2):
        label.setText(text)
        label.setAutoFillBackground(True)
        if (value < thresh1):
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorRed+"); }")
        elif ((value >= thresh1) and (value < thresh2)):
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
        else:
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")


    def updateLabelColorBoolean(self, label, text, boolFlag):
        label.setText(text)
        label.setAutoFillBackground(True)
        if (boolFlag):
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")
        else:
            label.setStyleSheet("QLabel { background-color: rgba("+self.bgColorRed+"); }")


    #---------------------------------------------------------------------------
    # Methods for interface update

    def updateBatteryLevel(self):
        if (self.gcsgui.mavint.currMavStatus == None):
            return
        self.updateLabelColorValueInvertRange(self.batteryLevelLabel,
                                        ' Battery: {0} %'.format(self.gcsgui.mavint.currMavStatus.battery_level),
                                        self.gcsgui.mavint.currMavStatus.battery_level,
                                        25, 50)


    def updateOnboardCPUStatus(self):
        if (self.gcsgui.mavint.currMavHwStatus == None):
            return
        self.updateLabelColorValueRange(self.cpuUsageLabel,
                                        ' CPU: {0}%'.format(self.gcsgui.mavint.currMavHwStatus.cpu_usage),
                                        self.gcsgui.mavint.currMavHwStatus.cpu_usage,
                                        60, 90)

        self.updateLabelColorValueRange(self.ramUsageLabel,
                                        ' RAM: {0}%'.format(self.gcsgui.mavint.currMavHwStatus.ram_usage),
                                        self.gcsgui.mavint.currMavHwStatus.ram_usage,
                                        60, 90)

        self.updateLabelColorValueRange(self.storageUsageLabel, 
                                        ' DISK: {0}%'.format(self.gcsgui.mavint.currMavHwStatus.storage_usage),
                                        self.gcsgui.mavint.currMavHwStatus.storage_usage,
                                        80, 95)




#===============================================================================

class GcsSoftwareStatusPanel:

    def __init__(self, gcsGui):
        self.gcsgui = gcsGui

        self.widget = QtGui.QWidget()
        self.widget.setWindowTitle("AirLab GCS - Software Status") 
        self.widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)

        self.opModeLabel = QtGui.QLabel('Mode: ??', self.widget)
        self.flightStateLabel = QtGui.QLabel('State: ??', self.widget)
        self.taskProgressLabel = QtGui.QLabel('Progress: ??', self.widget)

        #self.opModeLabel.setGeometry(50, 50, 400, 50)
        #self.flightStateLabel.setGeometry(50, 100, 400, 50)
                
        f = QtGui.QFont("Arial", 18, QtGui.QFont.Bold)

        self.opModeLabel.setFont(f)
        self.flightStateLabel.setFont(f)
        self.taskProgressLabel.setFont(f)

        grid = QtGui.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(self.opModeLabel, 1, 0, 1, 1)
        grid.addWidget(self.flightStateLabel, 2, 0, 1, 1)
        grid.addWidget(self.taskProgressLabel, 3, 0, 1, 1)
        
        self.widget.setLayout(grid)
        
        #self.widget.resize(400, 250)
        self.widget.show()

        # Register action to close using CTRL+C 
        self.closeAction = QtGui.QAction('Close', self.widget)
        self.closeAction.setShortcut('Ctrl+C')
        self.closeAction.setStatusTip('Close application')
        self.closeAction.triggered.connect(self.close)

        self.bgColorRed    = "{r}, {g}, {b}, {a}".format(r=255, g=0, b=0, a=127)
        self.bgColorYellow = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)
        self.bgColorBlue   = "{r}, {g}, {b}, {a}".format(r=0, g=0, b=255, a=127)
        self.bgColorGreen  = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)
        self.bgColorGray   = "{r}, {g}, {b}, {a}".format(r=127, g=127, b=127, a=127)

        #Implementing timer to update interface
        self.ctimer = QtCore.QTimer()
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.update)
        QtCore.QMetaObject.connectSlotsByName(self.widget)
        self.ctimer.start(1000)


    def update(self):
        #if (self.gcsgui.mavint.currMavHwStatus == None):
        #    return
        self.updateOpModeAndFlightState()
        #self.drawImageState()


    def close(self):
        print 'GcsSoftwareStatusPanel::close()'
        self.gcsgui.app.exit()

    #---------------------------------------------------------------------------
    # Methods for interface update
    
    def updateOpModeAndFlightState(self):
        if (self.gcsgui.mavint.currMavStatus == None):
            return
        #print 'GcsSoftwareStatusPanel::updateOpModeAndFlightState()'
        self.opModeLabel.setText('  Mode: {0}'.format(getOpModeStr(self.gcsgui.mavint.currMavStatus.mode)))
        self.opModeLabel.setAutoFillBackground(True)
        if (self.gcsgui.mavint.currMavStatus.mode == FCSStatus.MANUAL):
            self.opModeLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorBlue+"); }")
        elif (self.gcsgui.mavint.currMavStatus.mode == FCSStatus.AUTONOMOUS):
            self.opModeLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
        else:
            self.opModeLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGray+"); }")
        

        self.flightStateLabel.setText('  State: {0}'.format(getFlightStateStr(self.gcsgui.mavint.currMavStatus.state)))
        self.flightStateLabel.setAutoFillBackground(True)
        if (self.gcsgui.mavint.currMavStatus.state == FCSStatus.ONGROUND):
            self.flightStateLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")
        else:
            self.flightStateLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
        

        if ((self.gcsgui.mavint.currMavTaskStatus == None) or (not self.gcsgui.mavint.gcsHasMission)):
            self.taskProgressLabel.setText(" Progress : NO TASK")
            self.taskProgressLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
            return
        num_waypoints = int(self.gcsgui.mavint.currMavTaskStatus.data[0])
        if (num_waypoints == 0):
            self.taskProgressLabel.setText(" Progress : NO TASK")
            self.taskProgressLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorYellow+"); }")
            return
        waypoint_index = int(self.gcsgui.mavint.currMavTaskStatus.data[1])
        progress = int(self.gcsgui.mavint.currMavTaskStatus.data[2]*100.0)
        taskProgressStr = "  Progress : {0} %  -  ({1} / {2})".format(progress, waypoint_index, num_waypoints)
        self.taskProgressLabel.setText(taskProgressStr)
        self.taskProgressLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")


    def drawImageState(self):
    
        self.pixmap = QtGui.QPixmap(400, 400)

        pen = QtGui.QPen()
        pen.setWidth(3)
        pen.setColor(QtCore.Qt.red)    
    
        self.painter = QtGui.QPainter()
        self.painter.begin(self.pixmap)
        self.painter.setPen(pen)

        f = QtGui.QFont("Arial", 20, QtGui.QFont.Bold);
        self.painter.setFont(f)

        self.painter.drawEllipse(0, 0, 300, 300)
            
        self.painter.end()
        self.imgState.setPixmap(self.pixmap)
        self.widget.show()
    
#===============================================================================

class GcsControlButtonsPannel:

    def __init__(self, gcsGui):
        self.gcsgui = gcsGui

        self.widget = QtGui.QWidget()
        self.widget.setWindowTitle("AirLab GCS - Control") 
        self.widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.widget.closeEvent = self.closeEvent

        # Create labels        
        
        f = QtGui.QFont("Arial", 16, QtGui.QFont.Bold);

        #self.mavCommStatusLabel = QtGui.QLabel('MAV Comms: ??')
        #self.mavCommStatusLabel.setFont(f);

        #self.missionStatusLabel = QtGui.QLabel('Mission: ??')
        #self.missionStatusLabel.setFont(f);

        #self.spacer1 = QtGui.QLabel(self.widget)
        #self.spacer1.resize(150, 100)

        #self.spacer2 = QtGui.QLabel(self.widget)
        #self.spacer2.resize(100, 100)

        #self.spacer3 = QtGui.QLabel(self.widget)
        #self.spacer3.resize(150, 100)

        self.userFeedback = QtGui.QLabel(self.widget)
        self.userFeedback.setGeometry(50, 100, 1500, 40)
        self.userFeedback.setFont(f)
        self.userFeedback.setText(QtCore.QString('--'))


        #-----------------------------------------------------------------------
        # Create buttons

        f = QtGui.QFont("Arial", 24, QtGui.QFont.Bold);
        
        #btn = QtGui.QPushButton('Teleop ON', self.widget)
        #btn.setToolTip('Switch Teleop mode ON/OFF')
        #btn.setFont(f)
        #btn.clicked.connect(self.teleopButtonPressed)
        #btn.resize(btn.sizeHint())
        #self.btnTeleop = btn


        btn = QtGui.QPushButton('Take-off/Land', self.widget)
        btn.setToolTip('Take-off/Land command button')
        btn.setFont(f)
        btn.clicked.connect(self.takeoffLandPressed)
        #btn.resize(btn.sizeHint())
        btn.setGeometry(300, 15, 350, 75)
        self.btnTakeoffLand = btn

        btn = QtGui.QPushButton('Start/Stop', self.widget)
        btn.setToolTip('Start/Stop command button')
        btn.setFont(f)
        btn.clicked.connect(self.startStopPressed)
        #btn.resize(btn.sizeHint())
        btn.setGeometry(900, 15, 350, 75)
        self.btnStartStop = btn

        #btn = QtGui.QPushButton('Pause/Resume', self.widget)
        #btn.setToolTip('Pause/Resume command button')
        #btn.setFont(f)
        #btn.clicked.connect(self.pauseResumePressed)
        #btn.resize(btn.sizeHint())
        #self.btnPauseResume = btn

        #btn = QtGui.QPushButton('Close', self.widget)
        #btn.setToolTip('Close GCS Application')
        #btn.setFont(f)
        #btn.clicked.connect(self.close)
        #btn.resize(btn.sizeHint())
        #self.btnClose = btn

        #grid = QtGui.QGridLayout()
        #grid.setSpacing(10)

        #grid.addWidget(self.mavCommStatusLabel, 1, 0, 1, 2)
        #grid.addWidget(self.missionStatusLabel, 2, 0, 1, 2)
        #grid.addWidget(self.btnTeleop, 3, 0, 1, 2)
        #grid.addWidget(self.spacer1, 1, 0, 2, 1)
        #grid.addWidget(self.btnTakeoffLand, 1, 1, 2, 1)
        #grid.addWidget(self.spacer2, 1, 2, 2, 1)
        #grid.addWidget(self.btnStartStop, 1, 3, 2, 1)
        #grid.addWidget(self.spacer3, 1, 4, 2, 1)
        #grid.addWidget(self.btnPauseResume, 5, 1, 1, 1)
        #grid.addWidget(self.btnClose, 4, 1, 1, 1)
        #grid.addWidget(self.userFeedback, 3, 0, 1, 5)

        
        #self.widget.setLayout(grid)

        #self.widget.resize(1500, 250)
        self.widget.show()

        # Register action to close using CTRL+C 
        self.closeAction = QtGui.QAction('Close', self.widget)
        self.closeAction.setShortcut('Ctrl+C')
        self.closeAction.setStatusTip('Close application')
        self.closeAction.triggered.connect(self.close)

        self.bgColorRed    = "{r}, {g}, {b}, {a}".format(r=255, g=0, b=0, a=127)
        self.bgColorGreen  = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)

        #Implementing timer to update interface
        self.ctimer = QtCore.QTimer()
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.update)
        QtCore.QMetaObject.connectSlotsByName(self.widget)
        self.ctimer.start(100)


    def update(self):
        
        #self.setCommsStatus()
        
        #self.setMissionStatus()
        
        if (self.gcsgui.mavint.currMavStatus == None):
            return
            
        if (self.gcsgui.mavint.currMavStatus.mode == FCSStatus.MANUAL):
            self.setUIManualMode()
        else:
            if (self.gcsgui.mavint.gcsHasMission):                
                if (self.gcsgui.mavint.currMavStatus.state == FCSStatus.HOLDINGPOSITION):
                    self.setUIAutoModeHoldPosition()
                elif (self.gcsgui.mavint.currMavStatus.state == FCSStatus.EXECUTINGCOMMAND):
                    self.setUIAutoModeFollowTrajectory()
                elif (self.gcsgui.mavint.currMavStatus.state == FCSStatus.ONGROUND):
                    self.setUIAutoModeOnGround()
            else:
                self.setUIAutoModeNoMission()

    def closeEvent(self, event):
        print 'GcsControlButtonsPannel::closeEvent()'
        event.accept()
        self.close()

    def close(self):
        print 'GcsControlButtonsPannel::close()'
        self.gcsgui.app.exit()


    #---------------------------------------------------------------------------
    # Methods for user event handling
    '''
    def teleopButtonPressed(self):
        print 'GcsGui::teleopButtonPressed()'
        if (self.gcsgui.teleopActive):
            self.btnTeleop.setText('Teleop ON')
            self.gcsgui.setTeleopState(False)
            self.gcsgui.mavint.sendUserCmdTeleopOff()
        else:
            self.btnTeleop.setText('Teleop OFF')
            self.gcsgui.setTeleopState(True)
            self.gcsgui.mavint.sendUserCmdTeleopOn()
    '''

    def takeoffLandPressed(self):
        print 'GcsControlButtonsPannel::takeoffLandPressed()'
        if (self.gcsgui.mavint.mavIsOnGround):
            self.gcsgui.mavint.sendUserCmdTakeoff()
        else:
            self.gcsgui.mavint.sendUserCmdLand()
    
    
    def startStopPressed(self):
        print 'GcsControlButtonsPannel::startStopPressed()'
        if (self.gcsgui.mavint.currMavStatus.state == FCSStatus.HOLDINGPOSITION):
            self.gcsgui.mavint.sendPath()
            time.sleep(0.25)
            self.gcsgui.mavint.sendUserCmdStart()
        else:
            self.gcsgui.mavint.sendUserCmdStop()
        

    #def pauseResumePressed(self):
    #    print 'GcsGui::pauseResumePressed()'
    #    if (self.gcsgui.mavint.currMavStatus.state == FCSStatus.HOLDINGPOSITION):
    #        self.gcsgui.mavint.sendUserCmdResume()
    #    else:
    #        self.gcsgui.mavint.sendUserCmdPause()
    

    #---------------------------------------------------------------------------
    # Methods for enable/disable UI elements
    '''
    def setCommsStatus(self):
        self.mavCommStatusLabel.setText('MAV Comms')
        self.mavCommStatusLabel.setAutoFillBackground(True)
        if (self.gcsgui.mavint.lostCommsMAV):
            self.mavCommStatusLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorRed+"); }")
        else:
            self.mavCommStatusLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")

    def setMissionStatus(self):
        self.missionStatusLabel.setAutoFillBackground(True)
        if (self.gcsgui.mavint.gcsHasMission):
            self.missionStatusLabel.setText('Mission: READY')
            self.missionStatusLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorGreen+"); }")
        else:
            self.missionStatusLabel.setText('Mission: NOT AVAILABLE')
            self.missionStatusLabel.setStyleSheet("QLabel { background-color: rgba("+self.bgColorRed+"); }")
    '''
    def setUIManualMode(self):
        #self.btnTeleop.setEnabled(False)
        self.btnTakeoffLand.setEnabled(False)
        self.btnTakeoffLand.setText('---')
        self.btnStartStop.setEnabled(False)
        self.btnStartStop.setText('---')
        #self.btnPauseResume.setEnabled(False)
        #self.btnPauseResume.setText('---')
        self.userFeedback.setText("Manual Control - User Interface Disabled.")
    
    def setUITeleopMode(self):
        if (self.gcsgui.mavint.mavIsOnGround):
            self.btnTakeoffLand.setText('Take-off')
        else:
            self.btnTakeoffLand.setText('Land')
        self.btnTakeoffLand.setEnabled(True)
        self.btnStartStop.setEnabled(False)
        self.btnStartStop.setText('---')
        #self.btnPauseResume.setEnabled(False)
        #self.btnPauseResume.setText('---')
        self.userFeedback.setText("--")
    
    def setUIAutoModeOnGround(self):
        #self.btnTeleop.setEnabled(True)
        self.btnTakeoffLand.setText('Take-off')
        self.btnTakeoffLand.setEnabled(True)
        self.btnStartStop.setText('No Task')
        self.btnStartStop.setEnabled(False)
        #self.btnPauseResume.setText('---')
        #self.btnPauseResume.setEnabled(False)
        self.userFeedback.setText("Press 'Takeoff' to go to hover point")

    def setUIAutoModeFollowTrajectory(self):
        #self.btnTeleop.setEnabled(True)
        self.btnTakeoffLand.setText('Land')
        self.btnTakeoffLand.setEnabled(False)
        self.btnStartStop.setEnabled(False)
        self.btnStartStop.setText('Busy')
        #self.btnPauseResume.setEnabled(True)
        #self.btnPauseResume.setText('Pause')
        self.userFeedback.setText("Executing task...")
    
    def setUIAutoModeHoldPosition(self):
        #self.btnTeleop.setEnabled(True)
        self.btnTakeoffLand.setEnabled(True)
        self.btnTakeoffLand.setText('Land')
        self.btnStartStop.setEnabled(True)
        self.btnStartStop.setText('Start')
        #self.btnPauseResume.setEnabled(True)
        #self.btnPauseResume.setText('Resume')
        self.userFeedback.setText("Press 'Start' to execute task")

    def setUIAutoModeNoMission(self):
        #self.btnTeleop.setEnabled(True)
        if (self.gcsgui.mavint.mavIsOnGround):
            self.btnTakeoffLand.setText('Take-off')
        else:
            self.btnTakeoffLand.setText('Land')
        self.btnTakeoffLand.setEnabled(True)
        self.btnStartStop.setEnabled(False)
        self.btnStartStop.setText('No Task')
        #self.btnPauseResume.setEnabled(False)
        #self.btnPauseResume.setText('---')
        if (self.gcsgui.mavint.mavIsOnGround):
            self.userFeedback.setText("Press 'Takeoff' to go hover point")
        else:
            self.userFeedback.setText("Select area to be inspected or press 'Land'")
        

#===============================================================================

class GcsSystemStatusImagePanel:

    def __init__(self, gcsGui, image_list):
        self.gcsgui = gcsGui

        self.widget = QtGui.QWidget()
        self.widget.setWindowTitle("AirLab GCS - System State Image") 
        self.widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.widget.closeEvent = self.closeEvent
  
        self.width = 200
        self.height = 200

        self.pic = QtGui.QLabel(self.widget)
        self.pic.setGeometry(75, 40, self.width, self.height)

        self.url = QtGui.QLabel(self.widget)
        self.url.setGeometry(5, 50+self.height, 340, 50)
        self.url.setText(QtCore.QString("theairlab.org"))

        f = QtGui.QFont("Arial", 28, QtGui.QFont.Bold)
        self.url.setFont(f)
        self.url.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        
        self.widget.resize(self.width, self.height)
        self.widget.show()
        
        # Register action to close using CTRL+C 
        self.closeAction = QtGui.QAction('Close', self.widget)
        self.closeAction.setShortcut('Ctrl+C')
        self.closeAction.setStatusTip('Close application')
        self.closeAction.triggered.connect(self.close)

        self.bgColorRed    = "{r}, {g}, {b}, {a}".format(r=255, g=0, b=0, a=127)
        self.bgColorGreen  = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)

        #Implementing timer to update interface
        self.ctimer = QtCore.QTimer()
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.update)
        QtCore.QMetaObject.connectSlotsByName(self.widget)
        self.ctimer.start(100)
        
        self.logo_filename =  self.gcsgui.mavint.getAirLabLogoImageFilename()
        
        #Animation control vars
        self.takeoffStartTime = -1.0
        self.landStartTime = -1.0
        
        self.drawSystemStatusImage()


    def drawSystemStatusImage(self):
        self.pixmap = QtGui.QPixmap(self.logo_filename)
        self.pixmap = self.pixmap.scaled(200, 200, QtCore.Qt.KeepAspectRatio)
    
        self.painter = QtGui.QPainter()
        self.painter.begin(self.pixmap)

        self.painter.end()
        self.pic.setPixmap(self.pixmap)
        self.widget.show()


    def update(self):
        #if (self.selected_section != -1):
        #    if ((self.gcsgui.mavint.currMavStatus.state == FCSStatus.HOLDINGPOSITION) and
        #        (self.gcsgui.mavint.gcsHasMission == False)):
        #        #self.drawSystemStatusImage()
        return
        

    def closeEvent(self, event):
        print 'GcsControlAreaSelection::closeEvent()'
        event.accept()
        self.close()


    def close(self):
        print 'GcsControlAreaSelection::close()'
        self.gcsgui.app.exit()


#===============================================================================

class GcsControlAreaSelectionPanel:

    def __init__(self, gcsGui, area_img_filenames):
        self.gcsgui = gcsGui

        self.widget = QtGui.QWidget()
        self.widget.setWindowTitle("AirLab GCS - Area Selection") 
        self.widget.closeEvent = self.closeEvent
        self.widget.mousePressEvent = self.getPos
        self.widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)

        self.lateral_margin = 250
        
        self.selected_img = -1
        self.selected_section = -1
  
        self.img_width = 1024
        self.img_height = 768
  
        self.width = self.img_width
        self.height = self.img_height

        side = self.img_height / 5

        self.num_x_sections = 4
        self.num_y_sections = 3
        self.offset_x = (self.img_width - self.num_x_sections * side)/2
        self.offset_y = (self.img_height - self.num_y_sections * side)/6 + 75
        
        #self.space_x = (self.img_width - 2*self.offset_x) / self.num_x_sections
        self.space_x = 140 
        self.space_y = self.space_x

        self.img_filename1 = area_img_filenames[0]
        self.img_filename2 = area_img_filenames[1]

        self.demoLabel_height = 100

        self.demoLabel = QtGui.QLabel(self.widget)
        self.demoLabel.setText(QtCore.QString("AirLab - Infrastructure Inspection"))
        self.demoLabel.setGeometry(self.lateral_margin, 0, self.img_width, self.demoLabel_height)

        f = QtGui.QFont("Arial", 35, QtGui.QFont.Bold)
        self.demoLabel.setFont(f)
        self.demoLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        self.pic1 = QtGui.QLabel(self.widget)
        self.pic1.setGeometry(self.lateral_margin, self.demoLabel_height, self.img_width, self.img_height)
        
        #self.pic2 = QtGui.QLabel(self.widget)
        #self.pic2.setGeometry(self.lateral_margin+self.img_width, self.demoLabel_height, self.img_width, self.img_height)
        
        self.drawAreasForSelection()
        
        # Register action to close using CTRL+C 
        self.closeAction = QtGui.QAction('Close', self.widget)
        self.closeAction.setShortcut('Ctrl+C')
        self.closeAction.setStatusTip('Close application')
        self.closeAction.triggered.connect(self.close)

        self.bgColorRed    = "{r}, {g}, {b}, {a}".format(r=255, g=0, b=0, a=127)
        self.bgColorGreen  = "{r}, {g}, {b}, {a}".format(r=0, g=255, b=0, a=127)

        #Implementing timer to update interface
        self.ctimer = QtCore.QTimer()
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.update)
        QtCore.QMetaObject.connectSlotsByName(self.widget)
        self.ctimer.start(100)


    def getPos(self , event):
        if (self.gcsgui.mavint.currMavStatus.state == FCSStatus.EXECUTINGCOMMAND):
            return
            
        x = event.pos().x()
        y = event.pos().y() 
        print 'Pos: x={0} / y={1}'.format(x, y)
        
        sel = self.getSectionNumber(x, y)
        self.selected_img = sel[0]
        self.selected_section = sel[1]
        self.gcsgui.mavint.setPathSection(self.selected_img, self.selected_section)
        
        self.drawAreasForSelection()
        

    def getSectionNumber(self, pos_x, pos_y):
        pos_y = pos_y - self.demoLabel_height
        pos_x = pos_x - self.lateral_margin
        
        img_index = 1
        #if (pos_x > self.img_width):
        #    img_index = 2
        #    pos_x = pos_x - self.img_width
        
        row    = int( math.floor((pos_y - self.offset_y)/self.space_y) )
        column = int( math.floor((pos_x - self.offset_x)/self.space_x) )
        if ((row>=0) and (row<self.num_y_sections) and (column>=0) and (column<self.num_x_sections)):
            num = column + self.num_x_sections * row + 1
            return [img_index, num]
        if ((pos_x > self.offset_x) and
            (pos_x < (self.offset_x + self.num_x_sections*self.space_x)) and
            (pos_y > (self.offset_y + self.num_y_sections*self.space_y + 25)) and
            (pos_y < (self.offset_y + self.num_y_sections*self.space_y + 25 + self.space_y/2))):
            return [img_index, 0]
        return [-1, -1]


    def drawAreasForSelection(self):
    
        self.pixmap1 = QtGui.QPixmap(self.img_filename1)

        self.painter1 = QtGui.QPainter()
        self.painter1.begin(self.pixmap1)

        self.drawAreaSelection(self.painter1, 1)
        
        self.painter1.end()
        self.pic1.setPixmap(self.pixmap1)


        #self.pixmap2 = QtGui.QPixmap(self.img_filename2)

        #self.painter2 = QtGui.QPainter()
        #self.painter2.begin(self.pixmap2)

        #self.drawAreaSelection(self.painter2, 2)

        #self.painter2.end()
        #self.pic2.setPixmap(self.pixmap2)

        self.widget.show()


    def drawAreaSelection(self, painter, img_number):
        
        f = QtGui.QFont("Arial", 20, QtGui.QFont.Bold);
        painter.setFont(f)

        pen = QtGui.QPen()
        pen.setWidth(1)
        pen.setColor(QtCore.Qt.white)    
        painter.setPen(pen)

        painter.drawRect(1, 1, self.img_width-2, self.img_height-2)

        pen.setWidth(2)
        pen.setColor(QtCore.Qt.red)    
        painter.setPen(pen)
        
        offset_x = self.offset_x
        offset_y = self.offset_y
        
        #if (self.selected_img == 2):
        #    offset_x = offset_x + self.img_width
        
        #Area selection grid
        for i in range(self.num_x_sections+1):
            painter.drawLine(offset_x + i*self.space_x, offset_y, offset_x + i*self.space_x, offset_y + self.num_y_sections*self.space_y)
        for i in range(self.num_y_sections+1):
           painter.drawLine(offset_x, offset_y + i*self.space_y, offset_x + self.num_x_sections*self.space_x, offset_y + i*self.space_y)

        pen.setWidth(6)
        painter.setPen(pen)
        
        #Full coverage box
        painter.drawRect(offset_x-25, offset_y-25, self.num_x_sections*self.space_x+50, self.num_y_sections*self.space_y+50)

        painter.drawRect(offset_x, offset_y + self.num_y_sections*self.space_y + 25, self.num_x_sections*self.space_x, self.space_y/2)
        painter.drawText(offset_x + 170, offset_y + self.num_y_sections*self.space_y + 65, QtCore.QString(str('Execute Full Coverage')))

        for i in range(self.num_y_sections):
            for j in range(self.num_x_sections):
                painter.drawText(offset_x + j*self.space_x + self.space_x/2, offset_y + i*self.space_y + self.space_y/2, QtCore.QString(str(i*self.num_x_sections + j + 1)))

        if (self.selected_img != img_number):
            return
        
        if (self.selected_section != -1):
            pen = QtGui.QPen()
            pen.setWidth(5)
            pen.setColor(QtCore.Qt.blue)
            painter.setPen(pen)
            if (self.selected_section == 0):
                pen.setWidth(5)
                pen.setColor(QtCore.Qt.yellow)
                painter.setPen(pen)
                painter.drawRect(offset_x-25+5, offset_y-25+5, self.num_x_sections*self.space_x+50-10, self.num_y_sections*self.space_y+50-10)
                painter.drawRect(offset_x+5, offset_y + self.num_y_sections*self.space_y + 25 + 5, self.num_x_sections*self.space_x -10, self.space_y/2 -10)
            else:
                index = self.selected_section - 1
                row    = int(math.floor(index / self.num_x_sections))
                column = int(math.fmod(index, self.num_x_sections))
                painter.drawRect(offset_x + column*self.space_x +5, 
                                      offset_y + row*self.space_y +5, 
                                      self.space_x -10, self.space_y -10)


    def update(self):
        if (self.selected_section != -1):
            if ((self.gcsgui.mavint.currMavStatus.state == FCSStatus.HOLDINGPOSITION) and
                (self.gcsgui.mavint.gcsHasMission == False)):
                self.selected_section = -1
                self.drawAreasForSelection()
        return
        

    def closeEvent(self, event):
        print 'GcsControlAreaSelection::closeEvent()'
        event.accept()
        self.close()


    def close(self):
        print 'GcsControlAreaSelection::close()'
        self.gcsgui.app.exit()



#===============================================================================

class GcsGui:

    def __init__(self, mavInterface):
        print 'GcsGui::constructor()'

        self.mavint = mavInterface
                        
        self.app = QtGui.QApplication(sys.argv)
        #self.screenRect = self.app.desktop().screenGeometry()

        #-----------------------------------------------------------------------
        # Create widgets
        self.hardwareStatusPanel = GcsHardwareStatusPanel(self)
        self.systemStateImgPanel = GcsSystemStatusImagePanel(self, [])
        self.softwareStatusPanel = GcsSoftwareStatusPanel(self)
        self.controlButtonsPanel = GcsControlButtonsPannel(self)
        self.areaSelectionPanel  = GcsControlAreaSelectionPanel(self, self.mavint.getInspectionAreaImageFilenames())

        #-----------------------------------------------------------------------
        # Position widgets

        self.areaSelectionPanel.widget.setGeometry(   5,  25, 1550, 900)
        self.controlButtonsPanel.widget.setGeometry(  5, 930, 1550, 150)

        self.systemStateImgPanel.widget.setGeometry(1556,  20, 359, 350)
        self.softwareStatusPanel.widget.setGeometry(1556, 371, 359, 420)
        self.hardwareStatusPanel.widget.setGeometry(1556, 792, 359, 270)

        #-----------------------------------------------------------------------
        # Set initial flags
        
        self.teleopActive = False
        self.mavint.setTeleopState(False)


    def run(self):
        print 'GcsGui::run() - ENTER'
        ret = self.app.exec_()
        print 'GcsGui::run() - EXIT'
        return ret

    def updateGUI(self):
        self.areaSelectionPanel.update()

    def close(self):
        print 'GcsGui::close()'
        self.app.exit()
        
    def setTeleopState(self, flag):
        self.teleopActive = flag
        self.mavint.setTeleopState(flag)


